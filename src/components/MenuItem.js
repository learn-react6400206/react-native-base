import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Colors from '@/constants/Color';

const MenuItem = ({item}) => {
  return (
    <View style={[styles.menu, item.divide && styles.divide]}>
      <Icon
        name={item.icon}
        size={15}
        color={Colors.BLACK}
        style={styles.menuIcon}
      />
      <Text style={styles.menuText}>{item.menu}</Text>
      <Icon
        name="chevron-right"
        size={12}
        color={Colors.GREY}
        style={styles.menuArrow}
      />
    </View>
  );
};

export default MenuItem;

const styles = StyleSheet.create({
  menu: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    position: 'relative',
    paddingTop: 8,
    paddingBottom: 8,
  },
  menuText: {
    fontSize: 16,
    color: 'black',
  },
  menuArrow: {
    position: 'absolute',
    right: 0,
  },
  menuIcon: {
    marginRight: 20,
    width: 20,
  },
  divide: {
    borderTopWidth: 1,
    borderColor: Colors.GRAY,
    marginTop: 8,
    paddingTop: 16,
  },
});
