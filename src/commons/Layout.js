/* eslint-disable react-native/no-inline-styles */
import {View} from 'react-native';
import React from 'react';

export const Column = props => {
  return (
    <View
      style={[
        {flexWrap: props.wrap ? 'wrap' : 'nowrap'},
        props.style,
        {
          flexDirection: 'column',
        },
      ]}>
      {props.children}
    </View>
  );
};

export const Row = props => {
  return (
    <View
      style={[
        {flexWrap: props.wrap ? 'wrap' : 'nowrap'},
        props.style,
        {
          flexDirection: 'row',
        },
      ]}>
      {props.children}
    </View>
  );
};
