const Colors = {
  RED: '#ff0000',
  GREY: '#808080',
  PRIMARY: '#406BB8',
  GRAY: '#dddddd',
  WHITE: '#FFFFFF',
  BLUE_DARK: '#243B66',
  BLACK: '#000000',
};

export default Colors;
