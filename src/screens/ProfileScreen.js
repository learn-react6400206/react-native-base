import 'react-native-gesture-handler';
import React from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MenuItem from '@/components/MenuItem';
import Colors from '@/constants/Color';

const ProfileScreen = () => {
  const onPress = () => {
    console.log('abc');
  };

  const PROFILE = {
    username: 'Ha Thao',
    mail: 'thaoha@amela.vn',
    avatar:
      'https://acms-bucket.s3.amazonaws.com/2022-07-14-02:41:08-provider-avatar-thao.ha2@amela.vn',
  };

  const MENU = [
    {
      icon: 'heart-o',
      menu: 'Favorites',
    },
    {
      icon: 'download',
      menu: 'Downloads',
    },
    {
      icon: 'globe',
      menu: 'Language',
      divide: true,
    },
    {
      icon: 'map-marker',
      menu: 'Location',
    },
  ];

  return (
    <View style={styles.container}>
      <View style={styles.heading}>
        <Icon name="chevron-left" size={20} color={Colors.BLACK} />
        <Text style={styles.headingTitle}>My profile</Text>
        <Icon name="cog" size={20} color={Colors.BLACK} />
      </View>

      <View style={styles.profile}>
        <View style={styles.avatar}>
          <Image
            source={{
              uri: PROFILE.avatar,
            }}
            style={styles.image}
          />
          <TouchableOpacity onPress={onPress} style={styles.upload}>
            <Icon name="camera" size={12} color={Colors.BLACK} />
          </TouchableOpacity>
        </View>
        <View style={styles.info}>
          <Text style={styles.username}>{PROFILE.username}</Text>
          <Text style={styles.mail}>{PROFILE.mail}</Text>
          <TouchableOpacity onPress={onPress} style={styles.button}>
            <Text style={styles.buttonText}>Edit Profile</Text>
          </TouchableOpacity>
        </View>
      </View>

      {MENU.map((item, index) => (
        <MenuItem key={index} item={item} />
      ))}
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 50,
  },
  headingTitle: {
    fontSize: 16,
    fontWeight: '600',
    color: Colors.BLACK,
    width: 100,
    textAlign: 'center',
  },
  profile: {
    marginTop: 30,
    marginBottom: 30,
    paddingLeft: 20,
    paddingRight: 20,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 100,
    height: 100,
    marginRight: 30,
    borderRadius: 50,
    position: 'relative',
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 50,
    overflow: 'hidden',
  },
  username: {
    fontSize: 14,
    fontWeight: '600',
    color: Colors.BLACK,
    marginBottom: 5,
  },
  mail: {
    fontSize: 12,
  },
  button: {
    backgroundColor: '#008000f5',
    borderColor: '#008000f5',
    borderWidth: 1,
    borderRadius: 6,
    padding: 5,
    minHeight: 35,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  buttonText: {
    color: Colors.WHITE,
    fontSize: 12,
    fontWeight: '600',
  },
  upload: {
    width: 26,
    height: 26,
    borderColor: Colors.GRAY,
    borderWidth: 1,
    borderRadius: 13,
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.WHITE,
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
});
