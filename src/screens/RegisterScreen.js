import React, {useRef, useCallback} from 'react';
import {SafeAreaView, Text} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';

const RegisterScreen = ({navigation}) => {
  const formRef = useRef(null);

  useFocusEffect(
    useCallback(() => {
      formRef.current && formRef.current.resetForm();
    }, []),
  );

  return (
    <SafeAreaView>
      <Text>This is register screen</Text>
    </SafeAreaView>
  );
};
export default RegisterScreen;
